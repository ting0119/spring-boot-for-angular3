package com.systex.springboot.model;

import javax.persistence.*;

@Entity
@Table(name = "task_table")
public class TaskTable {
    @Id
    @SequenceGenerator(name = "task_seq")
    @Column(name = "task_seq")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int task_seq;

    @Column(name = "task_name")
    private String task_name;

    @Column(name = "description")
    private String description;


    public int getId() {
        return task_seq;
    }
    public void setId(int id) {
        this.task_seq = id;
    }
    public String getName() {
        return task_name;
    }
    public void setName(String name) {
        this.task_name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

}
