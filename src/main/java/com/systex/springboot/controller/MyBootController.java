package com.systex.springboot.controller;

import com.systex.springboot.model.TaskTable;
import com.systex.springboot.repository.TaskTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

public class MyBootController {
    @Autowired
    private TaskTableRepository repository;

    @GetMapping(value = "/" )
    public ResponseEntity< List<TaskTable> > getAllTask() {
        List<TaskTable> taskTables = new ArrayList<>();
        repository.findAll().forEach(taskTables::add);
        return new ResponseEntity<>(taskTables, HttpStatus.OK);
    }

    @PostMapping(value = "/")
    public ResponseEntity<TaskTable> addTask(@RequestBody TaskTable inputTask){
        TaskTable t = repository.save(inputTask);
        return new ResponseEntity<>( t, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{task_seq}") //angular URL
    public ResponseEntity<HttpStatus> deleteTask(@PathVariable("task_seq") int task_seq){
        repository.deleteById(task_seq);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(value = "/{task_seq}")
    public ResponseEntity<TaskTable> updateTask( @RequestBody TaskTable task){

            return new ResponseEntity<>(repository.save(task), HttpStatus.OK);

    }


}
